﻿using lumberbot.Classes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
//using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace lumberbot
{
    /// <summary>
    /// Логика взаимодействия для WindowVision.xaml
    /// </summary>
    public partial class WindowVision : Window
    {
        public WindowVision()
        {
            InitializeComponent();
        }

        public void Update(Detector[][] data, Bitmap screen)
        {
            // Рисование зон определения на карте
            Bitmap btmap = screen.Clone(new Rectangle(0, 0, screen.Width, screen.Height), System.Drawing.Imaging.PixelFormat.DontCare);

            Graphics gr = Graphics.FromImage(btmap);
            
            gr.SmoothingMode = SmoothingMode.AntiAlias;

            foreach (Detector[] dd in data)
                foreach (Detector d in dd)
                    gr.DrawEllipse(new Pen(d.isBranch ? Color.Blue : Color.Black, 5), new Rectangle(d.x - 5, d.y - 5, 10, 10));


            ImageVision.Source = BitmapToImageSource(btmap);
        }

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }
    }
}

﻿using lumberbot.Classes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lumberbot
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        WindowVision windowVision = new WindowVision();
        Lumberjack lumberjack;

        public MainWindow()
        {
            InitializeComponent();
            lumberjack = new Lumberjack();
            lumberjack.Log += Lumberjack_Log;
            lumberjack.LogModel += Lumberjack_LogModel;
        }

        private void Lumberjack_LogModel(Detector[][] data, Bitmap screen)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, (Action)(() =>
            {
                string text = "";

                for (int i = 0; i <= data.GetUpperBound(0); i++)
                    text += $"{data[i][0].isBranch} | {data[i][1].isBranch}" + Environment.NewLine;

                TextBoxModel.Text = text;

                windowVision.Update(data, screen);
            }));
        }

        private void Lumberjack_Log(string message)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, (Action) (() =>
            {
                DateTime dt = DateTime.Now;
                string text = $"{dt.Hour}:{dt.Minute}:{dt.Second}.{dt.Millisecond} | {message} {Environment.NewLine}{TextBoxLog.Text}";
                TextBoxLog.Text = text;
            }));
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            lumberjack.Start();
        }

        private void ButtonStop_Click(object sender, RoutedEventArgs e)
        {
            lumberjack.Stop();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            windowVision.Show();
        }
    }
}

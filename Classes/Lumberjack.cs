﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace lumberbot.Classes
{
    class Lumberjack
    {
        public delegate void LogHandler(string message);
        public event LogHandler Log;

        public delegate void LogModelHandler(Detector[][] data, Bitmap screen);
        public event LogModelHandler LogModel;

        Thread thread;
        Mover mover = new Mover();
        bool isActive = false;
        Color colorBranch = Color.FromArgb(161, 116, 56);

        /// <summary>Обновление  в соответствии с данными на экране</summary>
        private Detector[][] GetBranchData(Bitmap screen)
        {
            int branchRows = 5;
            int xLeft = 874; // Кол-во точек от левого края скриншота для левой ветви
            int xRight = 1046; // для правой ветви
            int yStart = 620; // Начальная высота для дерева
            int yStep = 100; // Шаг для анализа

            Detector[][] data = new Detector[branchRows][];

            for (int row = 0; row < branchRows; row++)
            {
                data[row] = new Detector[2];

                for (int column = 0; column < 2; column++)
                {
                    int x = column == 0 ? xLeft : xRight;
                    int y = yStart - yStep * row;

                    data[row][column] = new Detector()
                    {
                        x = x,
                        y = y,
                        isBranch = IsBranch(screen, x, y)
                    };
                }
            }

            return data;
        }

        private bool IsBranch(Bitmap screen, int x, int y)
        {
            //return screen.GetPixel(x, y) == colorBranch;
            for (int yy = y - 70; yy <= y + 10; yy++)
                if (screen.GetPixel(x, yy) == colorBranch)
                    return true;

            return false;
        }

        /// <summary>Составление маршрута</summary>
        private List<Route> MakeRoutes(Bitmap screen)
        {
            Detector[][] branchData = GetBranchData(screen);
            LogModel(branchData, screen);

            return Helpers.GetRoutes(branchData);
        }


        /// <summary>Сделать группу движений</summary>
        private async Task MoveIteration()
        {
            Log("Анализ изображения...");
            Bitmap screen = Helpers.Screenshot();

            List<Route> routes = MakeRoutes(screen);

            foreach (Route route in routes)
            {
                Log($"{route.repeat} шагов по направлению {route.direction}");

                for (int i = 0; i < route.repeat; i++)
                {
                    await mover.Move(route.direction);
                }
            }
            await Task.Delay(80);
        }

        public async Task CirclerAsync()
        {
            while (isActive)
            {
                await MoveIteration();
            }
        }

        public void Start()
        {
            if (!isActive)
            {
                isActive = true;
                
                thread = new Thread(delegate()
                {
                    Log("Начало через 5 секунд..."); Thread.Sleep(1000);
                    Log("Начало через 4 секунды..."); Thread.Sleep(1000);
                    Log("Начало через 3 секунды..."); Thread.Sleep(1000);
                    Log("Начало через 2 секунды..."); Thread.Sleep(1000);
                    Log("Начало через 1 секунду..."); Thread.Sleep(1000);
                    _ = CirclerAsync();
                });
                thread.Start();
            }
        }

        public void Stop()
        {
            if (thread != null)
            {
                isActive = false;
                if (thread.Join(500) == false)
                    thread.Abort();

                thread = null;
            }
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lumberbot.Classes
{
    class Mover
    {
        private MoveDirection _previousDirection = MoveDirection.Unknown;
        private int _delaySame = 20;
        private int _delayDifferent = 20;
        private int _delayAfterPress = 20;

        public async Task Move(MoveDirection direction)
        {
            int delay = direction == _previousDirection
                ? _delaySame
                : _delayDifferent;

            await Task.Delay(delay);

            switch (direction)
            {
                case MoveDirection.Left:
                    Helpers.PressKey(0x25); await Task.Delay(_delayAfterPress);
                    Helpers.PressKey(0x25); await Task.Delay(_delayAfterPress);
                    break;
                case MoveDirection.Right:
                    Helpers.PressKey(0x27); await Task.Delay(_delayAfterPress);
                    Helpers.PressKey(0x27); await Task.Delay(_delayAfterPress);
                    break;
                default: throw new Exception("В метод передана некорректная кнопка");
            }

            _previousDirection = direction;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace lumberbot.Classes
{
    class Helpers
    {
        public static Route GetDepthFromZeroRow(Detector[][] data)
        {
            int rows = data.GetUpperBound(0) + 1; // TODO: GetLength

            MoveDirection direction = data[0][0].isBranch ? MoveDirection.Right : MoveDirection.Left;

            int repeat = rows;

            for (int row = 0; row < rows; row++)
                if (data[row][direction == MoveDirection.Left ? 0 : 1].isBranch)
                {
                    repeat = row;
                    break;
                }


            return new Route()
            {
                direction = direction,
                repeat = repeat
            };
        }

        public static List<Route> GetRoutes(Detector[][] data)
        {
            List<Route> routes = new List<Route>();

            Detector[][] dataLast = (Detector[][])data.Clone();

            while (dataLast.Length > 0)
            {
                Route route = GetDepthFromZeroRow(dataLast);
                routes.Add(route);
                dataLast = dataLast.Cast<Detector[]>().Skip(route.repeat).ToArray();
            }

            return routes;
        }

        public static Bitmap Screenshot()
        {
            double screenLeft = 0; // SystemParameters.VirtualScreenLeft;
            double screenTop = 0; // SystemParameters.VirtualScreenTop;
            double screenWidth = 1920; //SystemParameters.VirtualScreenWidth;
            double screenHeight = 1080; // SystemParameters.VirtualScreenHeight;

            Bitmap bmp = new Bitmap((int)screenWidth, (int)screenHeight);
            Graphics g = Graphics.FromImage(bmp);
            g.CopyFromScreen((int)screenLeft, (int)screenTop, 0, 0, bmp.Size);

            return bmp;
        }

        [DllImport("user32.dll")]
        private static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, uint dwExtraInfo);

        public static void PressKey(int code)
        {
            keybd_event((byte)code, 0, 0x0001 | 0, 0);
        }
    }
}

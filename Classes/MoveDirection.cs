﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lumberbot.Classes
{
    /// <summary>Направление движения</summary>
    enum MoveDirection
    {
        /// <summary>Неизвестно</summary>
        Unknown,

        /// <summary>Движение влево</summary>
        Left,

        /// <summary>Движение направо</summary>
        Right
    }
}

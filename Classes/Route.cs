﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lumberbot.Classes
{
    struct Route
    {
        public MoveDirection direction;
        public int repeat;
    }
}
